# kwmtak001-3088f-project
Our HAT is a motor driver with position feedback HAT. It is used to track the position of motors using hall effect sensors. It can also be used to regulate the speed of the motors.

BILL OF QUANTITIES:

LT1076 motor driver IC - R30
Capacitor - R5
Resisor - R6
Inductor - R5
LED - R8
Diode - R7
Motor - R100

